/** Graph.c
 * ===========================================================
 * Name: CS220, Spring 2019
 * Modified by: C3C David Thacker
 * Section: T7
 * Project: PEX4
 * Purpose: The implementation of a graph.
 * ===========================================================
 */

#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <math.h>
#include "graph.h"
#include "gameLogic.h"

/** -------------------------------------------------------------------
 * Create the memory needed to hold a graph data structure.
 * @param numberVertices the number of vertices in the graph
 * @param bytesPerNode the number of bytes used to represent a
 *                     single vertex of the graph
 * @return a pointer to a graph struct
 */
Graph * graphCreate(int numberVertices, int bytesPerNode) {
    Graph* graphPointer = malloc(sizeof(Graph*));
    graphPointer->numberVertices = numberVertices;
    graphPointer->vertices = malloc(numberVertices * bytesPerNode);
    graphPointer->edges = malloc(sizeof(int*)*numberVertices);
    for (int i = 0; i < numberVertices; ++i) {
        graphPointer->edges[i] = malloc(sizeof(int)*numberVertices);
        for (int j = 0; j < numberVertices; ++j) {
            graphPointer->edges[i][j] = NO_EDGE;

        }
    }

    return graphPointer;
}

/** -------------------------------------------------------------------
 * Delete a graph data structure
 * @param graph the graph to delete
 */
void graphDelete(Graph * graph) {
    for (int i = 0; i < graph->numberVertices; ++i) {
        free(graph->edges[i]);
    }
    graph->vertices = 0;
    free(graph);
}

/** -------------------------------------------------------------------
 * Set the state of an edge in a graph
 * @param graph the graph to modify
 * @param fromVertex the beginning vertex of the edge
 * @param toVertex the ending vertex of the edge
 * @param state the state of the edge
 */
void graphSetEdge(Graph * graph, int fromVertex, int toVertex, int state) {
    graph->edges[fromVertex][toVertex] = state;
    graph->edges[toVertex][fromVertex] = state;

}

/** -------------------------------------------------------------------
 * Get the state of an edge in a graph
 * @param graph the graph
 * @param fromVertex the starting vertex of the edge
 * @param toVertex the ending vertex of the edge
 * @return the state of the edge
 */
int graphGetEdge(Graph * graph, int fromVertex, int toVertex) {
    int state = graph->edges[fromVertex][toVertex];
    return state;
}