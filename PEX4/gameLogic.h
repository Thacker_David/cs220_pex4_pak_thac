/** gameLogic.h
 * ===========================================================
 * Name: CS220, Spring 2019
 * Modified by: C3C David Thacker
 * Section: T7
 * Project: PEX4
 * Purpose: The definition of data and functions needed to
 *          implement a Shannon Switching Game.
 * ===========================================================
 */

#ifndef GAME_LOGIC_H
#define GAME_LOGIC_H

#include "Graph.h"
#include "graphics.h"
#include <stdbool.h>
#define FALSE  0
#define TRUE   1

// The values stored in the edge adjacency matrix
#define NO_EDGE      0 // No edge exists
#define NORMAL_EDGE  1 // Normal edge
#define LOCKED_EDGE  2 // An edge selected by the "SHORT player"

// The types of graph vertices.
#define NORMAL_VERTEX        0
#define STARTING_VERTEX      1
#define ENDING_VERTEX        2



// A single vertex in a graph
typedef struct vertex {
    int x;     // (x,y) location of the vertex in the graphics window
    int y;
    int type;  // NORMAL_VERTEX, STARTING_VERTEX, or ENDING_VERTEX
    int visited;
} Vertex;

// A structure for transferring edge data between functions.
typedef struct edge {
    int fromVertex;
    int toVertex;
} Edge;


typedef struct pointerArray{
    int finalValue;
    int predV[100];
}PointerArray;

// The two players are called "cut" and "short". The "cut" player
// removes edges from the graph. The "short" player tries to select
// edges that create a path from the starting and ending nodes.
#define CUT_PLAYER   0
#define SHORT_PLAYER 1

// Each player's turn can be made from user input (HUMAN_PLAYER)
// or by computer logic (AI_PLAYER).
#define HUMAN_PLAYER 0
#define AI_PLAYER    1

// Game status
#define GAME_UNDERWAY 0
#define SHORT_WINS    1
#define CUT_WINS      2

// Game functions
void setPlayerType(int player, int type);
void setActivePlayer(int player);
void createVertices(Graph * graph, int max_X, int max_Y,
                    int margin, int minDistance, int randomizev);
void createEdges(Graph * graph, int minNumberEdges, int maxNumberEdges,
                 int windowWidth, int windowHeight, double edgeLengthPercent);
void printGraph(Graph *graph);
void drawGraph(Graph *graph, HWND hwnd, int labelVertices);
void doTurn(Graph *graph, int mouseX, int mouseY);
int BFS(Graph *graph);
int checkInArray(int* discoverSet, int num, int numDiscovered);
int shortBFS(Graph *graph);
int* AIBFS(Graph *graph, int* foundPath);
void dijkstras(Graph* graph, PointerArray* output);
int minDistance(Graph* graph, int dist[], bool boolArray[], int vertexValue);
int findOpenPath(int* foundPathArray, Graph *graph, int from, int to);
void getAdjacentVerticies(Graph* graph, int* adjacentArray, int currentVertex, int visitedArray[]);
int miniumDistance(int distance[], int numberNodes, int visitedArray[]);
void shortdijkstras(Graph *graph, PointerArray *output);
void shortGetAdjacentVerticies(Graph *graph, int *adjacentArray, int currentVertex, int visitedArray[]);

#endif // GAME_LOGIC_H